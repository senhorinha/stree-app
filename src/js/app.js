angular.module('stree', [
  'ngRoute',
  'ngSanitize',
  'mobile-angular-ui',
  'mobile-angular-ui.gestures',
  'ngScrollTo',
  'angular-carousel',
  'angularSpinner'])
.config(function($routeProvider) {
  $routeProvider.when('/', {templateUrl:'tree.html', controller: 'TreeController', reloadOnSearch: false});
  $routeProvider.when('/about', {templateUrl:'about.html', controller: 'AboutController', reloadOnSearch: false});
}).run(function($rootScope, utils) {
  $rootScope.utils = utils;
}).constant("colors", {
  "very_low": "#808080",
  "low": "#996666",
  "medium": "#BFFF00",
  "high": "#00FF00",
  "very_high": "#00FFFF",
  "offline": "#BE4646"
})
