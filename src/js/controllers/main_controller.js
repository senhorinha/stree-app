angular.module('stree').controller('MainController', function($window, $scope, $rootScope, SharedState){
  SharedState.initialize($rootScope, 'modalSemConexao');
  $window.addEventListener("offline", function(e) { $rootScope.Ui.turnOn('modalSemConexao') });
  $rootScope.$on('$routeChangeSuccess', function(){
    if($scope.utils.isOffline()) {
      $rootScope.Ui.turnOn('modalSemConexao')
    }
  });
});
