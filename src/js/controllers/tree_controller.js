angular.module('stree').controller('TreeController', function($scope, $http, $interval, colors){
  $interval(function() {
    $http.get("https://stree.herokuapp.com/fetch_last_check").success(function(data) {
      switch(data.level) {
        case "VERY_LOW":
          color = colors.very_low;
          break;
        case "LOW":
          color = colors.low;
          break;
        case "MEDIUM":
          color = colors.medium;
          break;
        case "HIGH":
          color = colors.high;
          break;
        case "VERY_HIGH":
          color = colors.very_high;
          break;
      }
      $scope.tree_style = ".trunk, .trunk div { background: "+color +";}";
    });
  }, 1000);
});
