angular.module('stree').directive('littleTree',  function() {
  return {
    restrict: 'E',
    templateUrl: '_little_tree.html'
  }
});
