angular.module('stree').directive('modalSemConexao',  function() {
  return {
    restrict: 'E',
    templateUrl: 'modal_sem_conexao.html',
    scope: {
      mensagem: '@mensagem',
    }
  }
});
