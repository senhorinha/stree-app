angular.module('stree').directive('tree',  function() {
  return {
    restrict: 'E',
    templateUrl: '_tree.html'
  }
});
