angular.module('stree').factory('utils', function($window) {
  return {

    openDeviceBrowser:  function(externalLinkToOpen) {
      window.open(externalLinkToOpen, '_system', 'location=yes');
    },

    isOffline: function() {
      return !$window.navigator.onLine;
    },
    
    isOnline: function() {
      return $window.navigator.onLine;
    }
  }
});
